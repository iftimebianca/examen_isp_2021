package Examen_2021;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subiect2 extends JFrame {
    JTextField textField1, textField2;
    JTextArea textArea;
    JButton button;

    Subiect2() {
        setTitle("Subiectul2-Iftime Elena-Bianca");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(350,350);
        setVisible(true);

        init();
    }

    public void init() {
        this.setLayout(null);

        textField1 = new JTextField();
        textField1.setBounds(20,20,100,20);

        textField2 = new JTextField();
        textField2.setBounds(20,60,100,20);

        button = new JButton("Click here");
        button.setBounds(20,100,100,20);
        button.addActionListener(new ButtonAction());

        textArea = new JTextArea();
        textArea.setBounds(20,140,100,100);

        add(textField1);
        add(textField2);
        add(textArea);
        add(button);
    }

    public static void main(String[] args) {
        new Subiect2();
    }

    class ButtonAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text1 = Subiect2.this.textField1.getText();
            String text2 = Subiect2.this.textField2.getText();

            int a = Integer.valueOf(text1);
            int b = Integer.valueOf(text2);
            int c = a + b;

            String result = String.valueOf(c);
            Subiect2.this.textArea.setText(result);
        }
    }
}
